'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var chalk = require('chalk');

var LegolizeGenerator = yeoman.generators.Base.extend({
	init: function () {
		this.pkg = yeoman.file.readJSON(path.join(__dirname, '../package.json'));

		this.on('end', function () {
			if ( !this.options['skip-install'] ) {
				this.npmInstall();
			}
		});
	},

	askFor: function () {
		var done = this.async();

		// have Yeoman greet the user
		console.log(this.yeoman);

		// replace it with a short and sweet description of your generator
		console.log(chalk.magenta('You\'re using the fantastic Legolize generator.'));

		var prompts = [
			{
				type: 'input',
				name: 'assetsPath',
				message: 'Where should we put legolize?',
				default: 'app/assets/less'
			},
			{
				type: 'confirm',
				name: 'areYouSure',
				message: 'Are you really sure my friend?',
				default: true
			}
		];

		this.prompt(prompts, function (props) {
			this.assetsPath = props.assetsPath;
			this.areYouSure = props.areYouSure;
			done();
		}.bind(this));
	},

	app: function () {
		this.directory('less', this.assetsPath);
	},
});

module.exports = LegolizeGenerator;
